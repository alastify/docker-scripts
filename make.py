import click
import json
import os


ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
MANIFEST = os.path.join(ROOT_DIR, 'manifest.json')
BUILD_DIR = os.path.join(ROOT_DIR, 'build')

FRAMEWORK_MAP = {
    'wagtail': 'django',
}


def res_mod(filename: str, subs: dict) -> str:
    output = []
    with open(filename, 'r') as fp:
        for line in fp.readlines():
            for search, replace in subs.items():
                output.append(line.replace('{{%s}}' % search, replace))

    return ''.join(output)


def save_res(filename: str, content: str):
    with open(filename, 'w') as fp:
        fp.write(content)
    fn, fe = os.path.splitext(filename)
    if fe in ('.sh', ):
        os.chmod(filename, 0o655)


def framework_map(name: str) -> str:
    if name in FRAMEWORK_MAP.keys():
        return FRAMEWORK_MAP[name]

    return name


@click.command()
@click.argument('framework', type=click.Choice(['django',  'wagtail']))
@click.argument('name', type=click.STRING)
def build(framework, name):
    with open(MANIFEST, 'r') as fm:
        manifest = json.load(fm)
    framework = framework_map(name=framework)
    for item in manifest[framework]:
        source = os.path.join(os.path.join(ROOT_DIR, framework), item)
        output = res_mod(filename=source, subs={'name': name})
        target = os.path.join(BUILD_DIR, item)
        save_res(filename=target, content=output)


def make_clean():
    for item in os.scandir(BUILD_DIR):
        if item.is_file():
            os.unlink(item.path)


if __name__ == "__main__":
    if not os.path.exists(BUILD_DIR):
        os.mkdir(BUILD_DIR)
    else:
        make_clean()
    build()
