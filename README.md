# docker-scripts

Just run ``python make.py`` and read the help.

## Commonly used commands

    $ pipenv install gunicorn
    $ pipenv lock -r > requirements.txt

Build docker image

    $ sudo docker build -t myproject .
