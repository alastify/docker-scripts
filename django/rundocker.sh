#!/usr/bin/env bash

sudo docker run \
  --detach \
  --restart always \
  --network dobr0 \
  --ip 172.18.0.100 \
  --mount type=bind,source=/srv/docker/{{name}}/static,target=/app/static \
  --mount type=bind,source=/srv/docker/{{name}}/media,target=/app/media \
  --mount source={{name}}-logs,target=/app/logs \
  {{name}}

#sudo docker image save {{name}} | gzip -c > {{name}}-image.tar.gz
#gunzip -c {{name}}-image.tar.gz | docker image load
