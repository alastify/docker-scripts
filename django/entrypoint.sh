#!/usr/bin/env bash

ROOT_DIR=$( cd "$( dirname "$0" )" && pwd )
cd "$ROOT_DIR" || exit

#if [ "$DATABASE" = "postgres" ]; then
#    echo "Waiting for postgres..."
#
#    while ! nc -z $DATABASE_HOST $DATABASE_PORT; do
#      sleep 0.1
#    done
#
#    echo "PostgreSQL started"
#fi

# Migrate the database
python manage.py migrate --noinput
# Generate locales
#python manage.py compilemessages  --settings={{name}}.settings.production
# Collect static files
python manage.py collectstatic --noinput

exec "$@"
